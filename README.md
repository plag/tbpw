# Tiny Banano Paper Wallet

#### *Print, create a wallet using Kalium or BananoVault, write the data, deposit some funds and give as a gift or use as a backup!*

![Example](example1.png "Example 1")

![Example](example2.png "Example 2")

1 - Print the pdf model (A4-en.pdf) or use the png (wallet-en.png) as you wish.

2 - Create your wallet using a trusty software like
- BananoVault (Web browser) - https://vault.banano.cc/
- Kalium (iOS/Android) - https://kalium.banano.cc/

3 - Copy the seed, the mnemonic phrase and the account to the corresponding fields.

4 - Don't forget to change the representative and add funds. After, may you can delete the wallet on software.

5 - Be very careful when generating QR-Codes for secret keys. Use only offline software such as Zint Barcode Generator (Linux, macOS and Windows) and print to a offline or dumb printer.
https://sourceforge.net/projects/zint/
http://www.zint.org.uk/
http://zint.github.io/

6 - Cut, fold and you're ready to go :) 

***

*THIS IS IMPORTANT*: do not reveal the seed or mnemonic words to anyone.

Verify the wallet balance by searching for the account (public key) using a network explorer such as https://creeper.banano.cc. To withdraw and receive the funds sync the wallet with your seed into BananoVault or Kalium.

See more about Banano in https://banano.cc. Be free to reuse, adapt, fork or made whatever you want with this tiny project.

Something is wrong? https://www.reddit.com/user/pingalimaoacucargelo

***

Did you like it? Feel free to buy me a coffee:

NANO: nano_1qbhe8rb967gd5kipjhseuj7xkfrhzfnouz45ggoudcrsc9gquyqh6stgmqg

![Nano](nanoqrcode.png "Nano")

BANANO: ban_1nm9z16fge1modnfh9o9i7o47uqs8kc4uwtx99no5naqx9drpkzfax73uwo7

![Banano](bananoqrcode.png "Banano")

MONERO: 45KENDEETubSgzhTHRxqUXM5SErXy1TWsYDQGQ6mx8sefZHyYDDJVhsMp8ihKGPc2DDVS2zyQwb8echy6iLeDnMJAnHzB5p

![Monero](moneroqrcode.png "Monero")


Try this gift cards too: https://banano.cc/paperwallet/


See others paper wallet templates:
- Monero: https://codeberg.org/plag/tmpw
- Nano: https://codeberg.org/plag/tnpw